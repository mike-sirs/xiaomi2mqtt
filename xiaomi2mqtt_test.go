package main

import (
	"reflect"
	"testing"
)

var allTests = []struct {
	in, out string
}{
	{`{"humidity":"7753"}`, `{"humidity":7753}`},
	{`{"temperature":"742"}`, `{"temperature":742}`},
	{`{"no_motion":"600"}`, `{"no_motion":600}`},
	{`{}`, `{}`},
	{`{"pressure":"98327"}`, `{"pressure":98327}`},
	{`{"voltage":2995,"temperature":"2372","humidity":"7155","pressure":"100006"}`, `{"temperature":2372,"humidity":7155,"pressure":100006,"voltage":2995}`},
}

func TestXiDataToByteJSON(t *testing.T) {
	for _, test := range allTests {
		switch res, err := xiDataToByteJSON(test.in); {
		case err != nil:
			t.Fatalf("Can't convert xiaomi payload: %v", err)
		case reflect.DeepEqual(res, []byte(test.out)):
		default:
			t.Fatalf("responce %s of type %T, want %s of type %T.",
				res, res, test.out, test.out)
		}
	}
}

func BenchmarkXiDataToByteJSON(b *testing.B) {
	for i := 0; i < b.N; i++ {
		for _, test := range allTests {
			xiDataToByteJSON(test.in)
		}
	}
}
