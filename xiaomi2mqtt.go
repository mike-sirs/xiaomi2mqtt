package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net"
	"net/url"
	"strconv"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

type (
	response struct {
		Cmd     string      `json:"cmd"`
		Model   string      `json:"model"`
		Sid     string      `json:"sid"`
		ShortID interface{} `json:"short_id"`
		Token   string      `json:"token,omitempty"`
		IP      string      `json:"ip,omitempty"`
		Port    string      `json:"port,omitempty"`
		Data    interface{} `json:"data"`
	}

	request struct {
		Cmd string `json:"cmd"`
		Sid string `json:"sid,omitempty"`
	}

	gateway struct {
		Addr  *net.UDPAddr
		Sid   string
		Token string
	}

	xiData struct {
		Temperature  interface{} `json:"temperature,omitempty"`
		Humidity     interface{} `json:"humidity,omitempty"`
		Pressure     interface{} `json:"pressure,omitempty"`
		NoMotion     interface{} `json:"no_motion,omitempty"`
		MagnetStatus string      `json:"status,omitempty"`
		Illumination int         `json:"illumination,omitempty"`
		RGB          int         `json:"rgb,omitempty"`
		Lux          interface{} `json:"lux,omitempty"`
		Battery      int         `json:"battery,omitempty"`
		Voltage      int         `json:"voltage,omitempty"`
	}

	object map[string]interface{}
)

var (
	conn     *net.UDPConn
	gateways map[string]gateway
	mqttURI  = flag.String("mqtt", "", "mqtt uri in format e.g `user:password@10.0.0.1:1883/xiaomi`")
	debug    = flag.Bool("debug", false, "print out message from xi sensors to stdout before modification.")
)

const (
	multicastIP     = "224.0.0.50"
	multicastPort   = "9898"
	maxDatagramSize = 8192
)

func (g *gateway) sendMessage(msg string) {
	sendMessage(g.Addr, msg)
}

func sendMessage(addr *net.UDPAddr, msg string) {
	req, err := json.Marshal(request{Cmd: msg})
	if err != nil {
		log.Fatal(err)
	}
	log.Println(string(req))
	log.Println(addr)
	conn.WriteMsgUDP([]byte(req), nil, addr)
}

func connHandler() {
	pingAddr, err := net.ResolveUDPAddr("udp", multicastIP+":"+multicastPort)
	if err != nil {
		log.Fatal(err)
	}
	sendMessage(pingAddr, "whois")
}

// xiDataToByteJSON convert data reseived from weather and motion sensors from str to int
func xiDataToByteJSON(s string) ([]byte, error) {
	var origMsg = &xiData{}

	err := json.Unmarshal([]byte(s), &origMsg)
	if err != nil {
		log.Panic(err)
	}

	// TODO: I should rewrite it and check type before convert
	if origMsg.Temperature != nil {
		origMsg.Temperature, err = strconv.Atoi(origMsg.Temperature.(string))
		if err != nil {
			log.Panic(err)
		}
	}
	if origMsg.Humidity != nil {
		origMsg.Humidity, err = strconv.Atoi(origMsg.Humidity.(string))
		if err != nil {
			log.Panic(err)
		}
	}
	if origMsg.Pressure != nil {
		origMsg.Pressure, err = strconv.Atoi(origMsg.Pressure.(string))
		if err != nil {
			log.Panic(err)
		}
	}
	if origMsg.Lux != nil {
		origMsg.Lux, err = strconv.Atoi(origMsg.Lux.(string))
		if err != nil {
			log.Panic(err)
		}
	}
	if origMsg.NoMotion != nil {
		origMsg.NoMotion, err = strconv.Atoi(origMsg.NoMotion.(string))
		if err != nil {
			log.Panic(err)
		}
	}
	return json.Marshal(origMsg)
}

func msgHandler(resp []byte, l int) (string, []byte) {
	r := response{}
	rawIn := json.RawMessage(resp[:l])

	err := json.Unmarshal(rawIn, &r)
	if err != nil {
		log.Fatal(err)
	}

	if *debug {
		fmt.Println(&r)
	}

	switch r.Cmd {
	case "iam":
		_, err := net.ResolveUDPAddr("udp", r.IP+":"+multicastPort)
		if err != nil {
			log.Fatal(err)
		}
		return "iam", []byte("{some iam call, need to find out}")

	case "report", "heartbeat":
		switch v := r.Data.(type) {
		case string:
			data, err := xiDataToByteJSON(v)
			if err != nil {
				log.Panic(err)
			}
			return "status/" + r.Model + "/" + r.Sid, data
		default:
			fmt.Printf("I don't know about type %T!\n", v)
		}
	}
	return r.Model, []byte("unknown case")
}

func serveMulticastUDP(a string, connectedHandler func(), msgHandler func(resp []byte, l int) (string, []byte), mqClient mqtt.Client, topic string) {
	addr, err := net.ResolveUDPAddr("udp", a)
	if err != nil {
		log.Fatal(err)
	}
	conn, err = net.ListenMulticastUDP("udp", nil, addr)
	if err != nil {
		log.Panic(err)
	}

	conn.SetReadBuffer(maxDatagramSize)
	for {
		b := make([]byte, maxDatagramSize)
		n, _, err := conn.ReadFromUDP(b)
		if err != nil {
			log.Fatal("ReadFromUDP failed:", err)
		}

		device, res := msgHandler(b, n)
		mqClient.Publish(topic+"/"+device, 0, false, res)
		if !mqClient.IsConnected() {
			fmt.Println("Mqtt lost connection, reconnecting")
			mqClient.Connect()
		}
	}
}

// Mqtt
func connect(clientID string, uri *url.URL) mqtt.Client {
	opts := createClientOptions(clientID, uri)
	client := mqtt.NewClient(opts)
	token := client.Connect()
	for !token.WaitTimeout(3 * time.Second) {
	}
	if err := token.Error(); err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Mqtt is connected: %t. Topic: %s.\n", client.IsConnected(), uri.Path[1:len(uri.Path)])
	return client
}

func createClientOptions(clientID string, uri *url.URL) *mqtt.ClientOptions {
	opts := mqtt.NewClientOptions()
	opts.AddBroker(fmt.Sprintf("tcp://%s", uri.Host))
	opts.SetUsername(uri.User.Username())
	password, _ := uri.User.Password()
	opts.SetPassword(password)
	opts.SetClientID(clientID)
	return opts
}

func listen(uri *url.URL, topic string) {
	client := connect("sub", uri)
	client.Subscribe(topic, 0, func(client mqtt.Client, msg mqtt.Message) {
		fmt.Printf("* [%s] %s\n", msg.Topic(), string(msg.Payload()))
	})

}

func main() {
	flag.Parse()
	u, err := url.Parse("mqtt://" + *mqttURI)
	if err != nil {
		log.Fatal(err)
	}

	topic := u.Path[1:len(u.Path)]
	if topic == "" {
		topic = "test"
	}

	go listen(u, topic)
	client := connect("pub", u)

	serveMulticastUDP(multicastIP+":"+multicastPort, connHandler, msgHandler, client, topic)
}
