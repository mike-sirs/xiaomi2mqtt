[![pipeline status](https://gitlab.com/mike-sirs/xiaomi2mqtt/badges/master/pipeline.svg)](https://gitlab.com/mike-sirs/xiaomi2mqtt/-/commits/master)
[![coverage report](https://gitlab.com/mike-sirs/xiaomi2mqtt/badges/master/coverage.svg)](https://gitlab.com/mike-sirs/xiaomi2mqtt/-/commits/master)

# Xiaomi to MQTT bridge

Purpose: send data from Xiaomi gateway to MQTT broker. E.g if your HASSIO is in the cloud you need some bridge to forward data from Xi to MQTT.


- multicastIP     = "224.0.0.50"
- multicastPort   = "9898"

## How to build
- MAC OS
```
GOOS=darwin GOARCH=amd64 go build -o xiaomi2mqtt xiaomi2mqtt.go
```

- Linux
```
GOOS=linux GOARCH=amd64 go build go build -o xiaomi2mqtt xiaomi2mqtt.go
```

- Raspberry
```
GOARCH=arm7 GOOS=linux go build go build -o xiaomi2mqtt xiaomi2mqtt.go
```

## How to use
Run some where inside of you homenet where multicast is working

```bash
./xiaomi2mqtt -mqtt user:password@10.0.0.1:1883/xiaomi
```

E.G Topics

```
'mainTopic'/status/'model name'/'device sid'
xiaomi/status/gateway/04cf8c918146
body: {"illumination":384}

xiaomi/status/sensor_ht/158d0002556128
body: {"humidity":7753} or {"temperature":742}
```

## Hassio.

```
# configuration.yaml
mqtt:
  broker: 10.0.0.10
  username: user
  password: password

sensor:
  - platform: mqtt
    name: "Temperature-Room"
    state_topic: "xiaomi/status/sensor_ht/158d0002556128"
    icon: mdi:temperature-celsius
    unit_of_measurement: '°C'
    value_template: "{{ value_json.temperature / 100 }}"
  